# -*- coding: utf-8 -*-
from flask import Flask, render_template
import models

app = Flask(__name__)
app.config.from_object('config')
@app.route('/')
@app.route('/dashboard/')
def dashboard():
    data = models.Entity.get()
    return render_template('dashboard.htm', data=data)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
