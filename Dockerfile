FROM python:3.8-alpine
RUN pip install --upgrade pip
COPY . /app
WORKDIR /app
EXPOSE 5000
RUN pip install --no-cache-dir -r requirements.txt
CMD ["python", "app.py"]
